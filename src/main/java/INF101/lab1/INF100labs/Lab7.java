package INF101.lab1.INF100labs;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Implement the methods removeRow and allRowsAndColsAreEqualSum.
 * These programming tasks was part of lab7 in INF100 fall 2022/2023. You can find
 * them here: https://inf100h22.stromme.me/lab/7/
 */
public class Lab7 {

    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
        ArrayList<ArrayList<Integer>> grid3 = new ArrayList<>();
        grid3.add(new ArrayList<>(Arrays.asList(1, 2, 3, 4)));
        grid3.add(new ArrayList<>(Arrays.asList(2, 3, 4, 1)));
        grid3.add(new ArrayList<>(Arrays.asList(3, 4, 1, 2)));
        grid3.add(new ArrayList<>(Arrays.asList(4, 1, 2, 3)));

        boolean equalSums3 = allRowsAndColsAreEqualSum(grid3);
        System.out.println(equalSums3); // true
    }   

    public static void removeRow(ArrayList<ArrayList<Integer>> grid, int row) {
        grid.remove(row);
    }

    public static boolean allRowsAndColsAreEqualSum(ArrayList<ArrayList<Integer>> grid) {
        ArrayList<Integer> rowSums = new ArrayList<>();
        for (ArrayList<Integer> row : grid) {
            int rowSum = 0;
            for (int column : row) {
                rowSum += column;
            }
            if (!rowSums.contains(rowSum)) {
                rowSums.add(rowSum);
            }
        }
        if (rowSums.size() > 1) {
            return false;
        }
        ArrayList<Integer> columnSums = new ArrayList<>();
        for (int i = 0; i <= (grid.get(0)).size() - 1; i += 1) {
            int columnSum = 0;
            for (ArrayList<Integer> row : grid) {
                int number = row.get(i);
                columnSum += number;
            }
            if (!columnSums.contains(columnSum)) {
                columnSums.add(columnSum);
            }
        }
        if (columnSums.size() > 1) {
            return false;
        }
        return true;
    }
}