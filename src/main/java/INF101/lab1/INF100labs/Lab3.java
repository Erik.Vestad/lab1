package INF101.lab1.INF100labs;

/**
 * Implement the methods multiplesOfSevenUpTo, multiplicationTable and crossSum.
 * These programming tasks was part of lab3 in INF100 fall 2022/2023. You can find them here: https://inf100h22.stromme.me/lab/3/
 */
public class Lab3 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
        
    }

    public static void multiplesOfSevenUpTo(int n) {
        for (int i = 7; i <= n; i += 7) {
            System.out.println(i);
        }
    }

    public static void multiplicationTable(int n) {
        for (int i = 1; i <= n; i += 1) {
            System.out.printf("%s: ", i);
            for (int b = 1; b <= n; b += 1) {
                System.out.printf("%s ", b * i);
            }
            System.out.println();
        }
    }

    public static int crossSum(int num) {
        int sum = 0;
        String numString = Integer.toString(num);
        char[] numArray = numString.toCharArray();
        for (char c : numArray) {
            int tall = Character.getNumericValue(c);
            sum += tall;
        }
        return sum;
    }

}