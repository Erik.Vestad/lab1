package INF101.lab1.rockPaperScissors;

import java.util.List;
import java.util.Arrays;
import java.util.Scanner;
import java.util.Random;

public class RockPaperScissors {
	
    public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    Random rand = new Random();
    
    public void run() {
        // TODO: Implement me :)
        while (true) {
            System.out.printf("Let's play round %s\n", roundCounter);
            String humanChoice = readInput("Your choice (Rock/Paper/Scissors)?");

            while (!rpsChoices.contains(humanChoice)) {
                System.out.printf("I do not understand %s. Could you try again?\n", humanChoice);
                humanChoice = readInput("Your choice (Rock/Paper/Scissors)?");
            }
            int arrayIndex = rand.nextInt(3);
            String computerChoice = rpsChoices.get(arrayIndex);

            boolean humanWin = false;

            if (humanChoice.equals(computerChoice)) {
                System.out.printf("Human chose %s, computer chose %s. It's a tie!\n", humanChoice, computerChoice);
                System.out.printf("Score: human %d, computer %d\n", humanScore, computerScore);
                String humanAnswer = readInput("Do you wish to continue playing? (y/n)?");
                if (humanAnswer.equals("n")) {
                    System.out.println("Bye bye :)");
                    break;
                }
                roundCounter += 1;
                continue;
            } 
            
            if (humanChoice.equals("rock") && computerChoice.equals("scissors")) {
                humanWin = true;
            } else if (humanChoice.equals("paper") && computerChoice.equals("rock")) {
                humanWin = true;
            } else if (humanChoice.equals("scissors") && computerChoice.equals("paper")) {
                humanWin = true;
            }
            if (humanWin == true) {
                humanScore += 1;
                System.out.printf("Human chose %s, computer chose %s. Human wins!\n", humanChoice, computerChoice);
            } else {
                computerScore += 1;
                System.out.printf("Human chose %s, computer chose %s. Computer wins!\n", humanChoice, computerChoice);
            }
            System.out.printf("Score: human %d, computer %d\n", humanScore, computerScore);
            String humanAnswer = readInput("Do you wish to continue playing? (y/n)?");
            if (humanAnswer.equals("n")) {
                System.out.println("Bye bye :)");
                break;
            }
            roundCounter += 1;
        }
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}